<?php

namespace Drupal\dsi_center\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Security\TrustedCallbackInterface;

class Center extends ControllerBase implements TrustedCallbackInterface {

  public function front() {
    return ['#markup' => t('front')];
  }

  public static function preRenderDsiCenterTray(array $element) {
    $menu_tree = \Drupal::service('toolbar.menu_tree');
    $parameters = new MenuTreeParameters();
    $parameters->setRoot('dsi_center.admin')->excludeRoot()->setMaxDepth(4)->onlyEnabledLinks();
    $tree = $menu_tree->load(NULL, $parameters);
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
      ['callable' => 'toolbar_tools_center_menu_navigation_links'],
    ];

    $tree = $menu_tree->transform($tree, $manipulators);
    $element['dsi_center_menu'] = $menu_tree->build($tree);

    return $element;
  }

  public static function trustedCallbacks() {
    return ['preRenderDsiCenterTray'];
  }

}
