<?php

namespace Drupal\dsi_center\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class NavigationForm extends FormBase {

  public function getFormId() {
    return 'dsi_center_navigation_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
