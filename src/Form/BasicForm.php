<?php

namespace Drupal\dsi_center\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class BasicForm extends FormBase {

  public function getFormId() {
    return 'dsi_center_basic_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $site_config = \Drupal::configFactory()->get('system.site');

    $form['site_title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $this->t('Site information'),
      '#suffix' => '<hr/>',
    ];

    $form['site_name'] = [
      '#type' => 'textfield',
      '#title' => t('Site name'),
      '#default_value' => $site_config->get('name'),
      '#required' => TRUE,
      '#description' => '站点名称，将显示在浏览器窗口标题等位置',
    ];

    $site_mail = $site_config->get('mail');
    if (empty($site_mail)) {
      $site_mail = ini_get('sendmail_from');
    }
    $form['site_mail'] = [
      '#type' => 'email',
      '#title' => t('Email address'),
      '#default_value' => $site_mail,
      '#description' => '管理员 E-mail，将作为系统发邮件的时候的发件人地址',
      '#required' => TRUE,
    ];

    $site_qq = '80285394';
    $form['site_qq'] = [
      '#type' => 'number',
      '#title' => 'QQ客服号码',
      '#default_value' => isset($site_qq) ?: '',
      '#description' => '需要设置QQ在线状态，http://wp.qq.com/set.html?from=dsicenter&uin=',
    ];
    $form['site_beian'] = [
      '#type' => 'textfield',
      '#title' => '备案代码',
      '#default_value' => isset($site_beian) ?: '',
      '#description' => '页面底部可以显示 ICP 备案信息，如果网站已备案，在此输入您的授权码，它将显示在页面底部，如果没有请留空',
    ];
    $form['site_statistic'] = [
      '#type' => 'textarea',
      '#title' => '网站第三方统计代码',
      '#default_value' => isset($site_statistic) ?: '',
      '#description' => '页面底部可以显示第三方统计,双击输入框可扩大/缩小',
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Save',
    ];
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $data = [
      'site_name' => $values['site_name'],
      'site_mail' => $values['site_mail'],
    ];
    $site_config = \Drupal::configFactory()->getEditable('system.site');
    foreach ($data as $key => $value) {
      $site_config->set($key, $values);
    }
    $site_config->save();
  }

}
